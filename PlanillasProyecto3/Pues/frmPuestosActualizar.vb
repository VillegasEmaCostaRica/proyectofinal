﻿Public Class frmPuestosActualizar
    Public Shared tipoFormularioPuestoValidar As String
    Public Shared empleadoConfianza As Boolean

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestosActualizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDatoConsulta.Focus()
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

    End Sub

    Private Sub EMPLEADO_CONFIANZACheckBox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMPLEADO_CONFIANZACheckBox.Enter
        ' EMPLEADO_CONFIANZACheckBox.Checked = True
    End Sub

    Private Sub btnGaurdar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGaurdar.Click

        If EMPLEADO_CONFIANZACheckBox.Checked = True Then
            empleadoConfianza = True
        Else
            empleadoConfianza = False

        End If

        Select Case frmPrincipal.nomForm
            Case "Guardar"
                Try
                    Me.PuestosTableAdapter.InsertPuesto(Integer.Parse(CODIGO_DE_PUESTOTextBox.Text), DESCRIPCIONTextBox.Text, DEPARTAMENTOTextBox.Text, Integer.Parse(SALARIO_UNICOTextBox.Text), empleadoConfianza, GRADO_ACADEMICOTextBox.Text)
                    Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
                    MsgBox("Se Guardo Correctamente los Datos del Puesto Nuevo ")
                Catch ex As Exception
                    MsgBox("No Se Guardo los Datos porque Coincide con un Codigo ya Guardado ")
                End Try
            Case "Actualizar"
                Try

                    Me.PuestosTableAdapter.UpdatePuesto(Integer.Parse(CODIGO_DE_PUESTOTextBox.Text), DESCRIPCIONTextBox.Text, DEPARTAMENTOTextBox.Text, Integer.Parse(SALARIO_UNICOTextBox.Text), empleadoConfianza, GRADO_ACADEMICOTextBox.Text, frmPuestosConsulta.codPuesto)
                    MsgBox("Se Guardo Correctamente La Actualizacion De los Datos ")
                Catch ex As Exception
                    MsgBox("Verifique El Codigo Que Sea Correcto")
                End Try

            Case Else

        End Select



       
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Try

            Dim codPuesto As Integer = Integer.Parse(txtDatoConsulta.Text)


            If Me.PuestosTableAdapter.FillByBuscarPorCodigo(Me.Planilla2DataSet.Puestos, codPuesto) = "0" Then
                MsgBox("No se Encuentra Ningun Dato Con este Codigo")
                txtDatoConsulta.SelectAll()
            End If

        Catch ex As Exception
            MsgBox("Verifique El Codigo Que Sea Correcto")
        End Try
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        tipoDeFormularioPuesto("Actualizar")

    End Sub

    Sub tipoDeFormularioPuesto(ByVal NomFormulario As String)


        Select Case NomFormulario
            Case "Actualizar"
                Me.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Actualizar"
            Case "Eliminar"
                frmPuestoEliminar.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Eliminar"
            Case Else

        End Select


    End Sub

End Class