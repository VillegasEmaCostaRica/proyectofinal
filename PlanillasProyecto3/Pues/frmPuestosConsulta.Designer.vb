﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPuestosConsulta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PuestosDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.txtDatoConsulta = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DatosEmpleadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarAjusteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeduccucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDeduccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDedueccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpcionesUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteríaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.PuestosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PuestosDataGridView
        '
        Me.PuestosDataGridView.AutoGenerateColumns = False
        Me.PuestosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PuestosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn5})
        Me.PuestosDataGridView.DataSource = Me.PuestosBindingSource
        Me.PuestosDataGridView.Location = New System.Drawing.Point(34, 246)
        Me.PuestosDataGridView.Name = "PuestosDataGridView"
        Me.PuestosDataGridView.Size = New System.Drawing.Size(643, 175)
        Me.PuestosDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "CODIGO DE PUESTO"
        Me.DataGridViewTextBoxColumn1.HeaderText = "CODIGO DE PUESTO"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "DESCRIPCION"
        Me.DataGridViewTextBoxColumn2.HeaderText = "DESCRIPCION"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DEPARTAMENTO"
        Me.DataGridViewTextBoxColumn3.HeaderText = "DEPARTAMENTO"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "SALARIO UNICO"
        Me.DataGridViewTextBoxColumn4.HeaderText = "SALARIO UNICO"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "EMPLEADO CONFIANZA"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "EMPLEADO CONFIANZA"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "GRADO ACADEMICO"
        Me.DataGridViewTextBoxColumn5.HeaderText = "GRADO ACADEMICO"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtDatoConsulta
        '
        Me.txtDatoConsulta.Location = New System.Drawing.Point(109, 40)
        Me.txtDatoConsulta.Name = "txtDatoConsulta"
        Me.txtDatoConsulta.Size = New System.Drawing.Size(100, 20)
        Me.txtDatoConsulta.TabIndex = 14
        Me.ToolTip1.SetToolTip(Me.txtDatoConsulta, "Digitar el Nombre Del Departamento")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 16)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Departamento:"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(252, 38)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 12
        Me.btnBuscar.Text = "&Buscar"
        Me.ToolTip1.SetToolTip(Me.btnBuscar, "Ver Los Puestos Guardados")
        Me.btnBuscar.UseCompatibleTextRendering = True
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDatoConsulta)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(158, 127)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(392, 87)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ingrese el Departamento a consultar"
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatosEmpleadoToolStripMenuItem, Me.PuestosToolStripMenuItem, Me.AjustesToolStripMenuItem, Me.DeduccucionesToolStripMenuItem, Me.PlanillaMensualToolStripMenuItem, Me.PlanillaBisemanalToolStripMenuItem, Me.OpcionesUsuarioToolStripMenuItem, Me.ReporteríaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(706, 24)
        Me.MenuStrip1.TabIndex = 45
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DatosEmpleadoToolStripMenuItem
        '
        Me.DatosEmpleadoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDatosToolStripMenuItem, Me.ConsultarDatosToolStripMenuItem, Me.ActualizarDatosToolStripMenuItem, Me.EliminarDatosToolStripMenuItem})
        Me.DatosEmpleadoToolStripMenuItem.Name = "DatosEmpleadoToolStripMenuItem"
        Me.DatosEmpleadoToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.DatosEmpleadoToolStripMenuItem.Text = "&Datos Empleado"
        '
        'IngresarDatosToolStripMenuItem
        '
        Me.IngresarDatosToolStripMenuItem.Name = "IngresarDatosToolStripMenuItem"
        Me.IngresarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.IngresarDatosToolStripMenuItem.Text = "&Ingresar Datos"
        '
        'ConsultarDatosToolStripMenuItem
        '
        Me.ConsultarDatosToolStripMenuItem.Name = "ConsultarDatosToolStripMenuItem"
        Me.ConsultarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ConsultarDatosToolStripMenuItem.Text = "&Consultar Datos"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ActualizarDatosToolStripMenuItem.Text = "&Actualizar Datos"
        '
        'EliminarDatosToolStripMenuItem
        '
        Me.EliminarDatosToolStripMenuItem.Name = "EliminarDatosToolStripMenuItem"
        Me.EliminarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.EliminarDatosToolStripMenuItem.Text = "&Eliminar Datos"
        '
        'PuestosToolStripMenuItem
        '
        Me.PuestosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPuestosToolStripMenuItem, Me.ConsultarPuestosToolStripMenuItem, Me.ActualizarPuestoToolStripMenuItem, Me.EliminarPuestoToolStripMenuItem})
        Me.PuestosToolStripMenuItem.Name = "PuestosToolStripMenuItem"
        Me.PuestosToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.PuestosToolStripMenuItem.Text = "&Puestos"
        '
        'IngresarPuestosToolStripMenuItem
        '
        Me.IngresarPuestosToolStripMenuItem.Name = "IngresarPuestosToolStripMenuItem"
        Me.IngresarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarPuestosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.IngresarPuestosToolStripMenuItem.Text = "&Ingresar Puestos"
        '
        'ConsultarPuestosToolStripMenuItem
        '
        Me.ConsultarPuestosToolStripMenuItem.Name = "ConsultarPuestosToolStripMenuItem"
        Me.ConsultarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarPuestosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ConsultarPuestosToolStripMenuItem.Text = "&Consultar Puestos"
        '
        'ActualizarPuestoToolStripMenuItem
        '
        Me.ActualizarPuestoToolStripMenuItem.Name = "ActualizarPuestoToolStripMenuItem"
        Me.ActualizarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarPuestoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ActualizarPuestoToolStripMenuItem.Text = "&Actualizar Puesto"
        '
        'EliminarPuestoToolStripMenuItem
        '
        Me.EliminarPuestoToolStripMenuItem.Name = "EliminarPuestoToolStripMenuItem"
        Me.EliminarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarPuestoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.EliminarPuestoToolStripMenuItem.Text = "&Eliminar Puesto"
        '
        'AjustesToolStripMenuItem
        '
        Me.AjustesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarAjustesToolStripMenuItem, Me.ConsultarAjustesToolStripMenuItem, Me.ActualizarAjusteToolStripMenuItem})
        Me.AjustesToolStripMenuItem.Name = "AjustesToolStripMenuItem"
        Me.AjustesToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.AjustesToolStripMenuItem.Text = "&Ajustes"
        '
        'IngresarAjustesToolStripMenuItem
        '
        Me.IngresarAjustesToolStripMenuItem.Name = "IngresarAjustesToolStripMenuItem"
        Me.IngresarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.IngresarAjustesToolStripMenuItem.Text = "&Ingresar Ajustes"
        '
        'ConsultarAjustesToolStripMenuItem
        '
        Me.ConsultarAjustesToolStripMenuItem.Name = "ConsultarAjustesToolStripMenuItem"
        Me.ConsultarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ConsultarAjustesToolStripMenuItem.Text = "&Consultar Ajustes"
        '
        'ActualizarAjusteToolStripMenuItem
        '
        Me.ActualizarAjusteToolStripMenuItem.Name = "ActualizarAjusteToolStripMenuItem"
        Me.ActualizarAjusteToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ActualizarAjusteToolStripMenuItem.Text = "&Actualizar Ajuste"
        '
        'DeduccucionesToolStripMenuItem
        '
        Me.DeduccucionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDeduccionesToolStripMenuItem, Me.ConsultarDedueccionesToolStripMenuItem})
        Me.DeduccucionesToolStripMenuItem.Name = "DeduccucionesToolStripMenuItem"
        Me.DeduccucionesToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.DeduccucionesToolStripMenuItem.Text = "&Deducciones"
        '
        'IngresarDeduccionesToolStripMenuItem
        '
        Me.IngresarDeduccionesToolStripMenuItem.Name = "IngresarDeduccionesToolStripMenuItem"
        Me.IngresarDeduccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.IngresarDeduccionesToolStripMenuItem.Text = "&Ingresar Deducciones"
        '
        'ConsultarDedueccionesToolStripMenuItem
        '
        Me.ConsultarDedueccionesToolStripMenuItem.Name = "ConsultarDedueccionesToolStripMenuItem"
        Me.ConsultarDedueccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ConsultarDedueccionesToolStripMenuItem.Text = "&Consultar Deducciones"
        '
        'PlanillaMensualToolStripMenuItem
        '
        Me.PlanillaMensualToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaMensualToolStripMenuItem, Me.ConsultarPlanillaMensualToolStripMenuItem})
        Me.PlanillaMensualToolStripMenuItem.Name = "PlanillaMensualToolStripMenuItem"
        Me.PlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.PlanillaMensualToolStripMenuItem.Text = "&Planilla Mensual"
        '
        'IngresarPlanillaMensualToolStripMenuItem
        '
        Me.IngresarPlanillaMensualToolStripMenuItem.Name = "IngresarPlanillaMensualToolStripMenuItem"
        Me.IngresarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.IngresarPlanillaMensualToolStripMenuItem.Text = "&Ingresar Planilla Mensual"
        '
        'ConsultarPlanillaMensualToolStripMenuItem
        '
        Me.ConsultarPlanillaMensualToolStripMenuItem.Name = "ConsultarPlanillaMensualToolStripMenuItem"
        Me.ConsultarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ConsultarPlanillaMensualToolStripMenuItem.Text = "&Consultar Planilla Mensual"
        '
        'PlanillaBisemanalToolStripMenuItem
        '
        Me.PlanillaBisemanalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaBisemanalToolStripMenuItem, Me.ConsultarPlanillaBisemanalToolStripMenuItem})
        Me.PlanillaBisemanalToolStripMenuItem.Name = "PlanillaBisemanalToolStripMenuItem"
        Me.PlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.PlanillaBisemanalToolStripMenuItem.Text = "&Planilla Bisemanal"
        '
        'IngresarPlanillaBisemanalToolStripMenuItem
        '
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Name = "IngresarPlanillaBisemanalToolStripMenuItem"
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Text = "&Ingresar Planilla Bisemanal"
        '
        'ConsultarPlanillaBisemanalToolStripMenuItem
        '
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Name = "ConsultarPlanillaBisemanalToolStripMenuItem"
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Text = "&Consultar Planilla Bisemanal"
        '
        'OpcionesUsuarioToolStripMenuItem
        '
        Me.OpcionesUsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarUsuarioToolStripMenuItem, Me.EliminarUsuarioToolStripMenuItem, Me.ActualizarUsuarioToolStripMenuItem, Me.ConsultarUsuarioToolStripMenuItem})
        Me.OpcionesUsuarioToolStripMenuItem.Name = "OpcionesUsuarioToolStripMenuItem"
        Me.OpcionesUsuarioToolStripMenuItem.Size = New System.Drawing.Size(112, 20)
        Me.OpcionesUsuarioToolStripMenuItem.Text = "&Opciones Usuario"
        '
        'AgregarUsuarioToolStripMenuItem
        '
        Me.AgregarUsuarioToolStripMenuItem.Name = "AgregarUsuarioToolStripMenuItem"
        Me.AgregarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.AgregarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.AgregarUsuarioToolStripMenuItem.Text = "&Agregar Usuario "
        '
        'EliminarUsuarioToolStripMenuItem
        '
        Me.EliminarUsuarioToolStripMenuItem.Name = "EliminarUsuarioToolStripMenuItem"
        Me.EliminarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.EliminarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.EliminarUsuarioToolStripMenuItem.Text = "&Eliminar Usuario"
        '
        'ActualizarUsuarioToolStripMenuItem
        '
        Me.ActualizarUsuarioToolStripMenuItem.Name = "ActualizarUsuarioToolStripMenuItem"
        Me.ActualizarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ActualizarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ActualizarUsuarioToolStripMenuItem.Text = "Ac&tualizar Usuario"
        '
        'ConsultarUsuarioToolStripMenuItem
        '
        Me.ConsultarUsuarioToolStripMenuItem.Name = "ConsultarUsuarioToolStripMenuItem"
        Me.ConsultarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.ConsultarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ConsultarUsuarioToolStripMenuItem.Text = "&Consultar Usuario"
        '
        'ReporteríaToolStripMenuItem
        '
        Me.ReporteríaToolStripMenuItem.Name = "ReporteríaToolStripMenuItem"
        Me.ReporteríaToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.ReporteríaToolStripMenuItem.Text = "&Reportería"
        '
        'frmPuestosConsulta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 447)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PuestosDataGridView)
        Me.Name = "frmPuestosConsulta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPuestosConsulta"
        CType(Me.PuestosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents PuestosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtDatoConsulta As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DatosEmpleadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarAjusteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeduccucionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDeduccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDedueccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpcionesUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteríaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
