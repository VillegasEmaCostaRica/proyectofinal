﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPuestosActualizar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim DEPARTAMENTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim EMPLEADO_CONFIANZALabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnGaurdar = New System.Windows.Forms.Button()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.DEPARTAMENTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.EMPLEADO_CONFIANZACheckBox = New System.Windows.Forms.CheckBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.txtDatoConsulta = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DatosEmpleadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarPuestoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarAjusteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeduccucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDeduccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDedueccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpcionesUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteríaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        DEPARTAMENTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        EMPLEADO_CONFIANZALabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(25, 46)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(52, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 0
        CODIGO_DE_PUESTOLabel.Text = "CODIGO:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(25, 72)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(83, 13)
        DESCRIPCIONLabel.TabIndex = 2
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'DEPARTAMENTOLabel
        '
        DEPARTAMENTOLabel.AutoSize = True
        DEPARTAMENTOLabel.Location = New System.Drawing.Point(25, 98)
        DEPARTAMENTOLabel.Name = "DEPARTAMENTOLabel"
        DEPARTAMENTOLabel.Size = New System.Drawing.Size(100, 13)
        DEPARTAMENTOLabel.TabIndex = 4
        DEPARTAMENTOLabel.Text = "DEPARTAMENTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(25, 124)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 6
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'EMPLEADO_CONFIANZALabel
        '
        EMPLEADO_CONFIANZALabel.AutoSize = True
        EMPLEADO_CONFIANZALabel.Location = New System.Drawing.Point(25, 152)
        EMPLEADO_CONFIANZALabel.Name = "EMPLEADO_CONFIANZALabel"
        EMPLEADO_CONFIANZALabel.Size = New System.Drawing.Size(69, 13)
        EMPLEADO_CONFIANZALabel.TabIndex = 8
        EMPLEADO_CONFIANZALabel.Text = "EMPLEADO:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(25, 180)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 10
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnGaurdar)
        Me.GroupBox1.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.GroupBox1.Controls.Add(DESCRIPCIONLabel)
        Me.GroupBox1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.GroupBox1.Controls.Add(DEPARTAMENTOLabel)
        Me.GroupBox1.Controls.Add(Me.DEPARTAMENTOTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox1.Controls.Add(EMPLEADO_CONFIANZALabel)
        Me.GroupBox1.Controls.Add(Me.EMPLEADO_CONFIANZACheckBox)
        Me.GroupBox1.Controls.Add(GRADO_ACADEMICOLabel)
        Me.GroupBox1.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(85, 135)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(321, 274)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Puesto"
        '
        'btnGaurdar
        '
        Me.btnGaurdar.Location = New System.Drawing.Point(118, 229)
        Me.btnGaurdar.Name = "btnGaurdar"
        Me.btnGaurdar.Size = New System.Drawing.Size(75, 23)
        Me.btnGaurdar.TabIndex = 6
        Me.btnGaurdar.Text = "&Guardar Actualización"
        Me.ToolTip1.SetToolTip(Me.btnGaurdar, "Guadar Puesto o Actualizacion De Puesto")
        Me.btnGaurdar.UseVisualStyleBackColor = True
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(164, 43)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.CODIGO_DE_PUESTOTextBox, "Codigo Del Puesto ")
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(164, 69)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONTextBox.TabIndex = 1
        '
        'DEPARTAMENTOTextBox
        '
        Me.DEPARTAMENTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DEPARTAMENTO", True))
        Me.DEPARTAMENTOTextBox.Location = New System.Drawing.Point(164, 95)
        Me.DEPARTAMENTOTextBox.Name = "DEPARTAMENTOTextBox"
        Me.DEPARTAMENTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DEPARTAMENTOTextBox.TabIndex = 2
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(164, 121)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 3
        '
        'EMPLEADO_CONFIANZACheckBox
        '
        Me.EMPLEADO_CONFIANZACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PuestosBindingSource, "EMPLEADO CONFIANZA", True))
        Me.EMPLEADO_CONFIANZACheckBox.Location = New System.Drawing.Point(164, 147)
        Me.EMPLEADO_CONFIANZACheckBox.Name = "EMPLEADO_CONFIANZACheckBox"
        Me.EMPLEADO_CONFIANZACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.EMPLEADO_CONFIANZACheckBox.TabIndex = 4
        Me.EMPLEADO_CONFIANZACheckBox.Text = "Si"
        Me.ToolTip1.SetToolTip(Me.EMPLEADO_CONFIANZACheckBox, "Si es Empleado De Confianza")
        Me.EMPLEADO_CONFIANZACheckBox.UseVisualStyleBackColor = True
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(164, 177)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 5
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnConsultar)
        Me.GroupBox2.Controls.Add(Me.txtDatoConsulta)
        Me.GroupBox2.Controls.Add(Me.btnBuscar)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(465, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(260, 220)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ingresar Codigo Puesto Para Actualizar"
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(150, 106)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(75, 39)
        Me.btnConsultar.TabIndex = 2
        Me.btnConsultar.Text = "&Consultar Puetos"
        Me.ToolTip1.SetToolTip(Me.btnConsultar, "Consultar Los Puestos Que Estan Guardados")
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'txtDatoConsulta
        '
        Me.txtDatoConsulta.Location = New System.Drawing.Point(125, 65)
        Me.txtDatoConsulta.Name = "txtDatoConsulta"
        Me.txtDatoConsulta.Size = New System.Drawing.Size(100, 20)
        Me.txtDatoConsulta.TabIndex = 0
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(57, 106)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 39)
        Me.btnBuscar.TabIndex = 1
        Me.btnBuscar.Text = "&Buscar"
        Me.ToolTip1.SetToolTip(Me.btnBuscar, "Buscar el Puesto Para Actualizar")
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Código Puesto:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatosEmpleadoToolStripMenuItem, Me.PuestosToolStripMenuItem, Me.AjustesToolStripMenuItem, Me.DeduccucionesToolStripMenuItem, Me.PlanillaMensualToolStripMenuItem, Me.PlanillaBisemanalToolStripMenuItem, Me.OpcionesUsuarioToolStripMenuItem, Me.ReporteríaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(815, 24)
        Me.MenuStrip1.TabIndex = 45
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DatosEmpleadoToolStripMenuItem
        '
        Me.DatosEmpleadoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDatosToolStripMenuItem, Me.ConsultarDatosToolStripMenuItem, Me.ActualizarDatosToolStripMenuItem, Me.EliminarDatosToolStripMenuItem})
        Me.DatosEmpleadoToolStripMenuItem.Name = "DatosEmpleadoToolStripMenuItem"
        Me.DatosEmpleadoToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.DatosEmpleadoToolStripMenuItem.Text = "&Datos Empleado"
        '
        'IngresarDatosToolStripMenuItem
        '
        Me.IngresarDatosToolStripMenuItem.Name = "IngresarDatosToolStripMenuItem"
        Me.IngresarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.IngresarDatosToolStripMenuItem.Text = "&Ingresar Datos"
        '
        'ConsultarDatosToolStripMenuItem
        '
        Me.ConsultarDatosToolStripMenuItem.Name = "ConsultarDatosToolStripMenuItem"
        Me.ConsultarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ConsultarDatosToolStripMenuItem.Text = "&Consultar Datos"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ActualizarDatosToolStripMenuItem.Text = "&Actualizar Datos"
        '
        'EliminarDatosToolStripMenuItem
        '
        Me.EliminarDatosToolStripMenuItem.Name = "EliminarDatosToolStripMenuItem"
        Me.EliminarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarDatosToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.EliminarDatosToolStripMenuItem.Text = "&Eliminar Datos"
        '
        'PuestosToolStripMenuItem
        '
        Me.PuestosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPuestosToolStripMenuItem, Me.ConsultarPuestosToolStripMenuItem, Me.ActualizarPuestoToolStripMenuItem, Me.EliminarPuestoToolStripMenuItem})
        Me.PuestosToolStripMenuItem.Name = "PuestosToolStripMenuItem"
        Me.PuestosToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.PuestosToolStripMenuItem.Text = "&Puestos"
        '
        'IngresarPuestosToolStripMenuItem
        '
        Me.IngresarPuestosToolStripMenuItem.Name = "IngresarPuestosToolStripMenuItem"
        Me.IngresarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IngresarPuestosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.IngresarPuestosToolStripMenuItem.Text = "&Ingresar Puestos"
        '
        'ConsultarPuestosToolStripMenuItem
        '
        Me.ConsultarPuestosToolStripMenuItem.Name = "ConsultarPuestosToolStripMenuItem"
        Me.ConsultarPuestosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ConsultarPuestosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ConsultarPuestosToolStripMenuItem.Text = "&Consultar Puestos"
        '
        'ActualizarPuestoToolStripMenuItem
        '
        Me.ActualizarPuestoToolStripMenuItem.Name = "ActualizarPuestoToolStripMenuItem"
        Me.ActualizarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ActualizarPuestoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ActualizarPuestoToolStripMenuItem.Text = "&Actualizar Puesto"
        '
        'EliminarPuestoToolStripMenuItem
        '
        Me.EliminarPuestoToolStripMenuItem.Name = "EliminarPuestoToolStripMenuItem"
        Me.EliminarPuestoToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EliminarPuestoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.EliminarPuestoToolStripMenuItem.Text = "&Eliminar Puesto"
        '
        'AjustesToolStripMenuItem
        '
        Me.AjustesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarAjustesToolStripMenuItem, Me.ConsultarAjustesToolStripMenuItem, Me.ActualizarAjusteToolStripMenuItem})
        Me.AjustesToolStripMenuItem.Name = "AjustesToolStripMenuItem"
        Me.AjustesToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.AjustesToolStripMenuItem.Text = "&Ajustes"
        '
        'IngresarAjustesToolStripMenuItem
        '
        Me.IngresarAjustesToolStripMenuItem.Name = "IngresarAjustesToolStripMenuItem"
        Me.IngresarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.IngresarAjustesToolStripMenuItem.Text = "&Ingresar Ajustes"
        '
        'ConsultarAjustesToolStripMenuItem
        '
        Me.ConsultarAjustesToolStripMenuItem.Name = "ConsultarAjustesToolStripMenuItem"
        Me.ConsultarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ConsultarAjustesToolStripMenuItem.Text = "&Consultar Ajustes"
        '
        'ActualizarAjusteToolStripMenuItem
        '
        Me.ActualizarAjusteToolStripMenuItem.Name = "ActualizarAjusteToolStripMenuItem"
        Me.ActualizarAjusteToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ActualizarAjusteToolStripMenuItem.Text = "&Actualizar Ajuste"
        '
        'DeduccucionesToolStripMenuItem
        '
        Me.DeduccucionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDeduccionesToolStripMenuItem, Me.ConsultarDedueccionesToolStripMenuItem})
        Me.DeduccucionesToolStripMenuItem.Name = "DeduccucionesToolStripMenuItem"
        Me.DeduccucionesToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.DeduccucionesToolStripMenuItem.Text = "&Deducciones"
        '
        'IngresarDeduccionesToolStripMenuItem
        '
        Me.IngresarDeduccionesToolStripMenuItem.Name = "IngresarDeduccionesToolStripMenuItem"
        Me.IngresarDeduccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.IngresarDeduccionesToolStripMenuItem.Text = "&Ingresar Deducciones"
        '
        'ConsultarDedueccionesToolStripMenuItem
        '
        Me.ConsultarDedueccionesToolStripMenuItem.Name = "ConsultarDedueccionesToolStripMenuItem"
        Me.ConsultarDedueccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ConsultarDedueccionesToolStripMenuItem.Text = "&Consultar Deducciones"
        '
        'PlanillaMensualToolStripMenuItem
        '
        Me.PlanillaMensualToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaMensualToolStripMenuItem, Me.ConsultarPlanillaMensualToolStripMenuItem})
        Me.PlanillaMensualToolStripMenuItem.Name = "PlanillaMensualToolStripMenuItem"
        Me.PlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.PlanillaMensualToolStripMenuItem.Text = "&Planilla Mensual"
        '
        'IngresarPlanillaMensualToolStripMenuItem
        '
        Me.IngresarPlanillaMensualToolStripMenuItem.Name = "IngresarPlanillaMensualToolStripMenuItem"
        Me.IngresarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.IngresarPlanillaMensualToolStripMenuItem.Text = "&Ingresar Planilla Mensual"
        '
        'ConsultarPlanillaMensualToolStripMenuItem
        '
        Me.ConsultarPlanillaMensualToolStripMenuItem.Name = "ConsultarPlanillaMensualToolStripMenuItem"
        Me.ConsultarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ConsultarPlanillaMensualToolStripMenuItem.Text = "&Consultar Planilla Mensual"
        '
        'PlanillaBisemanalToolStripMenuItem
        '
        Me.PlanillaBisemanalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaBisemanalToolStripMenuItem, Me.ConsultarPlanillaBisemanalToolStripMenuItem})
        Me.PlanillaBisemanalToolStripMenuItem.Name = "PlanillaBisemanalToolStripMenuItem"
        Me.PlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.PlanillaBisemanalToolStripMenuItem.Text = "&Planilla Bisemanal"
        '
        'IngresarPlanillaBisemanalToolStripMenuItem
        '
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Name = "IngresarPlanillaBisemanalToolStripMenuItem"
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Text = "&Ingresar Planilla Bisemanal"
        '
        'ConsultarPlanillaBisemanalToolStripMenuItem
        '
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Name = "ConsultarPlanillaBisemanalToolStripMenuItem"
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Text = "&Consultar Planilla Bisemanal"
        '
        'OpcionesUsuarioToolStripMenuItem
        '
        Me.OpcionesUsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarUsuarioToolStripMenuItem, Me.EliminarUsuarioToolStripMenuItem, Me.ActualizarUsuarioToolStripMenuItem, Me.ConsultarUsuarioToolStripMenuItem})
        Me.OpcionesUsuarioToolStripMenuItem.Name = "OpcionesUsuarioToolStripMenuItem"
        Me.OpcionesUsuarioToolStripMenuItem.Size = New System.Drawing.Size(112, 20)
        Me.OpcionesUsuarioToolStripMenuItem.Text = "&Opciones Usuario"
        '
        'AgregarUsuarioToolStripMenuItem
        '
        Me.AgregarUsuarioToolStripMenuItem.Name = "AgregarUsuarioToolStripMenuItem"
        Me.AgregarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.AgregarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.AgregarUsuarioToolStripMenuItem.Text = "&Agregar Usuario "
        '
        'EliminarUsuarioToolStripMenuItem
        '
        Me.EliminarUsuarioToolStripMenuItem.Name = "EliminarUsuarioToolStripMenuItem"
        Me.EliminarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.EliminarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.EliminarUsuarioToolStripMenuItem.Text = "&Eliminar Usuario"
        '
        'ActualizarUsuarioToolStripMenuItem
        '
        Me.ActualizarUsuarioToolStripMenuItem.Name = "ActualizarUsuarioToolStripMenuItem"
        Me.ActualizarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ActualizarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ActualizarUsuarioToolStripMenuItem.Text = "Ac&tualizar Usuario"
        '
        'ConsultarUsuarioToolStripMenuItem
        '
        Me.ConsultarUsuarioToolStripMenuItem.Name = "ConsultarUsuarioToolStripMenuItem"
        Me.ConsultarUsuarioToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.ConsultarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.ConsultarUsuarioToolStripMenuItem.Text = "&Consultar Usuario"
        '
        'ReporteríaToolStripMenuItem
        '
        Me.ReporteríaToolStripMenuItem.Name = "ReporteríaToolStripMenuItem"
        Me.ReporteríaToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.ReporteríaToolStripMenuItem.Text = "&Reportería"
        '
        'frmPuestosActualizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(815, 421)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmPuestosActualizar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPuestosActualizar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEPARTAMENTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMPLEADO_CONFIANZACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGaurdar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDatoConsulta As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DatosEmpleadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarPuestoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarAjusteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeduccucionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDeduccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDedueccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpcionesUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteríaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
