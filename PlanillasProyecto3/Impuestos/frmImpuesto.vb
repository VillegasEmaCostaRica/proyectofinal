﻿Public Class frmImpuesto
    Private opcion As Integer

    Private Sub frmConsultaImpuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'TODO: This line of code loads data into the 'Planilla2DataSet.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
        limpiarCasillas()

    End Sub

    Private Sub validarCajasTexo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CodImpuestoTextBox.KeyPress, ProcentajeImpuesto1TextBox.KeyPress, ProcentajeImpuesto2TextBox.KeyPress, CreditosFiscalesHijoTextBox.KeyPress, CreditosFiscalesConyugeTextBox.KeyPress

        Dim objeto As TextBox
        objeto = CType(sender, TextBox)

        If (Not Char.IsNumber(e.KeyChar) And e.KeyChar <> Microsoft.VisualBasic.ChrW(8)) Then
            e.Handled = True
            ToolStripStatusLabel1.Text = "Sólo se ingresa número en el campo"
            ErrorProvider1.SetError(objeto, "Sólo se ingresa número en el campo")
            Beep()
        Else
            ErrorProvider1.Clear()
            ToolStripStatusLabel1.Text = "Status"
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.ImpuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub MostarHistorialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MostarHistorialToolStripMenuItem.Click
        Me.Close()
        frmConsultaImpuesto.Show()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        agregarDATOS()
    End Sub


    Sub agregarDATOS()
        Try
            If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
                MsgBox("No pueden haber campos vacios")
            Else
                ImpuestosTableAdapter.InsertQuery(Integer.Parse(CodImpuestoTextBox.Text), Integer.Parse(ProcentajeImpuesto1TextBox.Text), Integer.Parse(ProcentajeImpuesto2TextBox.Text), Integer.Parse(CreditosFiscalesHijoTextBox.Text), Integer.Parse(CreditosFiscalesHijoTextBox.Text))
                MsgBox("Datos almacenados exitosamente")
                limpiarCasillas()
            End If
            
        Catch ex As Exception
            MsgBox("Verifique los datos de entrada sean validos o que no se repita un año de impuesto")
        End Try
        
    End Sub

    Private Sub limpiarCasillas()
        CodImpuestoTextBox.Text = 0
        ProcentajeImpuesto1TextBox.Text = 0
        ProcentajeImpuesto2TextBox.Text = 0
        CreditosFiscalesHijoTextBox.Text = 0
        CreditosFiscalesConyugeTextBox.Text = 0
    End Sub

    Private Sub ArchivoToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchivoToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Archivo"
    End Sub

    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        agregarDATOS()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        agregarDATOS()
    End Sub

    Private Sub AccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccionesToolStripMenuItem.Click

    End Sub

    Private Sub btnGuardar_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnGuardar.MouseMove
        ToolStripStatusLabel1.Text = "Guardar"
    End Sub

    Private Sub btnGuardar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"

    End Sub


    Private Sub Button2_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.MouseHover
        ToolStripStatusLabel1.Text = "Modificar"
    End Sub

    Private Sub Button2_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub Button3_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseHover
        ToolStripStatusLabel1.Text = "Eliminar"
    End Sub

    Private Sub Button3_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub ArchivoToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchivoToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub AccionesToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccionesToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Acciones"
    End Sub

    Private Sub AccionesToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccionesToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub ConsultarToolStripMenuItem_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarToolStripMenuItem.MouseHover
        ToolStripStatusLabel1.Text = "Consultar"
    End Sub

    Private Sub ConsultarToolStripMenuItem_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarToolStripMenuItem.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Sub modificar()

        ImpuestosTableAdapter.UpdateQuery(Integer.Parse(CodImpuestoTextBox.Text), Integer.Parse(ProcentajeImpuesto1TextBox.Text), Integer.Parse(ProcentajeImpuesto2TextBox.Text), Integer.Parse(CreditosFiscalesHijoTextBox.Text), Integer.Parse(CreditosFiscalesConyugeTextBox.Text), Integer.Parse(CodImpuestoTextBox.Text))
        limpiarCasillas()

        Try
          
        Catch ex As ConstraintException
            MsgBox("Verifique los datos de entrada" + ex.Message)

        End Try       
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click

        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
            If MsgBox("Seguro decea modificar el registro", vbYesNo) = vbYes Then
                modificar()
                limpiarCasillas()
            End If
        End If

    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Me.Hide()
        frmConsultaImpuesto.Show()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
            If MsgBox("Seguro decea modificar el registro", vbYesNo) = vbYes Then
                modificar()
                limpiarCasillas()
            End If
        End If

    End Sub


    Sub eliminarRegistro()
        opcion = CodImpuestoTextBox.Text
        Try
            If MsgBox("Seguro desea eliminar el registro", vbYesNo) = vbYes Then
                ImpuestosTableAdapter.DeleteQuery(Integer.Parse(opcion))
                MsgBox("Registro eliminado")
                limpiarCasillas()
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
            eliminarRegistro()
            Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
            limpiarCasillas()
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click

        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
                eliminarRegistro()
        End If

    End Sub

    Private Sub ModificarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarToolStripMenuItem.Click
        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
            If MsgBox("Seguro decea modificar el registro", vbYesNo) = vbYes Then
                modificar()
                limpiarCasillas()
            End If
        End If

    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        If CodImpuestoTextBox.Text = "0" Or ProcentajeImpuesto1TextBox.Text = "0" Or ProcentajeImpuesto2TextBox.Text = "0" Or CreditosFiscalesHijoTextBox.Text = "0" Or CreditosFiscalesConyugeTextBox.Text = "0" Then
            MsgBox("No pueden haber campos vacios")
        Else
            eliminarRegistro()
            Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
            limpiarCasillas()
        End If
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        limpiarCasillas()

    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub
End Class