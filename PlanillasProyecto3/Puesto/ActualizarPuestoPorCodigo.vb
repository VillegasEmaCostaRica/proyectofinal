﻿Public Class ActualizarPuestoPorCodigo

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub ActualizarPuestoPorCodigo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        ActualizarPuestoCodigo()

    End Sub

    Private Sub ActualizarPuestoPorCodigo_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        Principal.Show()
    End Sub

    Private Sub CODIGO_DE_PUESTOTextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SALARIO_UNICOTextBox.Validating, GRADO_ACADEMICOTextBox.Validating, DESCRIPCIONTextBox.Validating, DEPARTAMENTOTextBox.Validating, CODIGO_DE_PUESTOTextBox.Validating
        Dim cajaTexto As TextBox = CType(sender, TextBox)


        If cajaTexto.Text = "" Then
            e.Cancel = True
            proveedorError.SetError(cajaTexto, "Campo Requerido")
            cajaTexto.Focus()
        End If
    End Sub
    Private Sub txtNom_ValidatinigNumeros(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles CODIGO_DE_PUESTOTextBox.Validating, SALARIO_UNICOTextBox.Validating

        Dim cajaTexto As TextBox = CType(sender, TextBox)
        Dim dato As Integer
        Try
            dato = Integer.Parse(cajaTexto.Text)
        Catch ex As Exception
            e.Cancel = True
            proveedorError.SetError(cajaTexto, "Campo Númerico")
            cajaTexto.Focus()
        End Try


    End Sub
    Private Sub CODIGO_DE_PUESTOTextBox_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox.Validated, GRADO_ACADEMICOTextBox.Validated, DESCRIPCIONTextBox.Validated, DEPARTAMENTOTextBox.Validated, CODIGO_DE_PUESTOTextBox.Validated
        proveedorError.Clear()
    End Sub
End Class