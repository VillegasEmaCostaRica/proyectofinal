﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActualizarPuesto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnActuCodPuesto = New System.Windows.Forms.Button()
        Me.btnActNomPuesto = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnActuCodPuesto
        '
        Me.btnActuCodPuesto.Location = New System.Drawing.Point(169, 96)
        Me.btnActuCodPuesto.Name = "btnActuCodPuesto"
        Me.btnActuCodPuesto.Size = New System.Drawing.Size(75, 49)
        Me.btnActuCodPuesto.TabIndex = 0
        Me.btnActuCodPuesto.Text = "Actualizar Por &Codigo Puesto"
        Me.btnActuCodPuesto.UseVisualStyleBackColor = True
        '
        'btnActNomPuesto
        '
        Me.btnActNomPuesto.Location = New System.Drawing.Point(315, 96)
        Me.btnActNomPuesto.Name = "btnActNomPuesto"
        Me.btnActNomPuesto.Size = New System.Drawing.Size(75, 49)
        Me.btnActNomPuesto.TabIndex = 1
        Me.btnActNomPuesto.Text = "Actualizar Por &Nombre Del Puesto"
        Me.btnActNomPuesto.UseVisualStyleBackColor = True
        '
        'ActualizarPuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(482, 261)
        Me.Controls.Add(Me.btnActNomPuesto)
        Me.Controls.Add(Me.btnActuCodPuesto)
        Me.Name = "ActualizarPuesto"
        Me.Text = "Eliminar Puesto"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnActuCodPuesto As System.Windows.Forms.Button
    Friend WithEvents btnActNomPuesto As System.Windows.Forms.Button
End Class
