﻿Public Class frmDatosEmpleadoConsulta

    Public datoCedula As Integer

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDatosEmpleadoConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)


    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Dim lleno As Integer = Me.DatosPersonalesTableAdapter.FillBy1BuscarSiEstaPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))


            If lleno <> 0 Then
                Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))
            Else
                MsgBox("No Hay Ningun Registro Con esta Cedula ")
                
            End If


        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    

    Private Sub DatosPersonalesDataGridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatosPersonalesDataGridView.DoubleClick
        'Dim cedula As Integer = Integer.Parse(DatosPersonalesDataGridView.Item(0, DatosPersonalesDataGridView.CurrentRow.Index).Value)
        datocedula = Integer.Parse(DatosPersonalesDataGridView.Item(0, DatosPersonalesDataGridView.CurrentRow.Index).Value)

        Select Case formdatosEmpleadosActualizar.tipoFormularioDatosPersonalesValidar
            Case "Actualizar"
                formdatosEmpleadosActualizar.DatosPersonalesTableAdapter.FillBy1BuscarPersona(formdatosEmpleadosActualizar.Planilla2DataSet.DatosPersonales, datoCedula)
                formdatosEmpleadosActualizar.Show()
            Case "Eliminar"
                frmDatosEmpleadoEliminar.DatosPersonalesTableAdapter.FillBy1BuscarPersona(frmDatosEmpleadoEliminar.Planilla2DataSet.DatosPersonales, datoCedula)
                frmDatosEmpleadoEliminar.txtDatoConsulta.Text = datoCedula
                frmDatosEmpleadoEliminar.Show()

            Case "Ajuste"

                frmAjustes.DatosPersonalesTableAdapter.FillBy1BuscarPersona(frmAjustes.Planilla2DataSet.DatosPersonales, datoCedula)
                ' frmAjustes.PuestosTableAdapter.FillBy1(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
                frmAjustes.Show()
                Me.Hide()

            Case Else

        End Select


    End Sub

   
End Class