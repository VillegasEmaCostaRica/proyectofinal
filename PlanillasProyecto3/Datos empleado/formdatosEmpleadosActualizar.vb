﻿Public Class formdatosEmpleadosActualizar
    Public Shared cedula As Integer
    Public Shared tipoFormularioDatosPersonalesValidar As String

    
    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub formdatosEmpleadosActualizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.

        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        ' Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

   
    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Me.Hide()
        TipoDeFormularioDatosResonales("Actualizar")

    End Sub
    Sub TipoDeFormularioDatosResonales(ByVal NomFormulario As String)


        Select Case NomFormulario

            Case "Actualizar"
                frmDatosEmpleadoConsulta.Show()
                tipoFormularioDatosPersonalesValidar = "Actualizar"

            Case "Eliminar"
                frmDatosEmpleadoConsulta.Show()
                tipoFormularioDatosPersonalesValidar = "Eliminar"

            Case Else

        End Select

    End Sub
    Private Sub Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        guardar()
       
        
    End Sub


    Private Sub IngresarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmPrincipal.nomForm = "Guardar"
        Me.Show()
        Me.btnBuscar.Enabled = False
        Me.btnConsultar.Enabled = False
        Me.txtDatoConsulta.Enabled = False
    End Sub


    Private Sub formdatosEmpleadosActualizar_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles TelefonoTextBox.Validating, NOMBRETextBox.Validating, GRADO_ACADEMICOTextBox.Validating, E_MAILTextBox.Validating, DireccionTextBox.Validating, CelularTextBox.Validating, BANCO_A_DEPOSITARTextBox.Validating, _2__APELLIDOTextBox.Validating, _1__APELLIDOTextBox.Validating
        Dim objTextBox As TextBox = CType(sender, TextBox)

        Try


            If objTextBox.Text = "" Then
                e.Cancel = True
                objTextBox.SelectAll()
                ErrorProvider1.SetError(objTextBox, "Campo Requerido")
            End If

        Catch ex As Exception

            e.Cancel = True
            objTextBox.SelectAll()
            ErrorProvider1.SetError(objTextBox, "Campo Requerido")


        End Try
    End Sub



    Private Sub NoCEDULATextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles NoCEDULATextBox.Validating, N__HIJOSTextBox.Validating


        Dim objTextBox As TextBox = CType(sender, TextBox)

        Try
            Dim num As Integer = Integer.Parse(objTextBox.Text)

        Catch ex As Exception

            e.Cancel = True
            objTextBox.SelectAll()
            ErrorProvider1.SetError(objTextBox, "Tiene que ser Numerico")


        End Try
    End Sub

    Private Sub Validatedtext(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TelefonoTextBox.Validated, NOMBRETextBox.Validated, NoCUENTATextBox.Validated, N__HIJOSTextBox.Validated, GRADO_ACADEMICOTextBox.Validated, E_MAILTextBox.Validated, DireccionTextBox.Validated, CelularTextBox.Validated, BANCO_A_DEPOSITARTextBox.Validated, _2__APELLIDOTextBox.Validated, _1__APELLIDOTextBox.Validated
        ErrorProvider1.Clear()
    End Sub

    Sub guardar()

        Dim fechaIngreso As DateTime = Convert.ToDateTime(FECHA_INGRESODateTimePicker.Text)
        Dim anosServicio As Integer = DateDiff(DateInterval.Year, fechaIngreso, Date.Now())

        Dim nombreCompleto As String = _1__APELLIDOTextBox.Text & " " & _2__APELLIDOTextBox.Text & " " & NOMBRETextBox.Text
        Dim casado As Boolean
        If N__CONYUGESCheckBox.Checked = True Then
            casado = True
        Else
            casado = True
        End If


        Select Case frmPrincipal.nomForm
            Case "Guardar"
                Try
                    Me.DatosPersonalesTableAdapter.InsertPersona(Integer.Parse(NoCEDULATextBox.Text), _1__APELLIDOTextBox.Text, _2__APELLIDOTextBox.Text, NOMBRETextBox.Text, nombreCompleto, E_MAILTextBox.Text, Integer.Parse(CODPUESTOTextBox.Text), FECHA_INGRESODateTimePicker.Text, 0, False, Integer.Parse(N__HIJOSTextBox.Text), GRADO_ACADEMICOTextBox.Text, BANCO_A_DEPOSITARTextBox.Text, NoCUENTATextBox.Text, TelefonoTextBox.Text, CelularTextBox.Text, DireccionTextBox.Text)
                    Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
                    MsgBox("Se Guardo Sastifactoriamente Los Datos De la Persona")
                Catch ex As Exception
                    MsgBox("No se Puede Guardar Los Datos Porque la cedula Coincide Con una cedula De una persona Registrada")
                End Try
            Case "Actualizar"
                Try

                    Me.DatosPersonalesTableAdapter.UpdateDatosPersona(Integer.Parse(NoCEDULATextBox.Text), _1__APELLIDOTextBox.Text, _2__APELLIDOTextBox.Text, NOMBRETextBox.Text, nombreCompleto, E_MAILTextBox.Text, Integer.Parse(CODPUESTOTextBox.Text), fechaIngreso, anosServicio, casado, Integer.Parse(N__HIJOSTextBox.Text), GRADO_ACADEMICOTextBox.Text, BANCO_A_DEPOSITARTextBox.Text, NoCUENTATextBox.Text, TelefonoTextBox.Text, CelularTextBox.Text, DireccionTextBox.Text, frmDatosEmpleadoConsulta.datoCedula)
                    Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
                    MsgBox("Se Guardo Sastifactoriamente Los Datos De la Persona")
                Catch ex As Exception
                    MsgBox("No se Puede Guardar Los Datos Porque la cedula Coincide Con una cedula De una persona Registrada")
                End Try

            Case Else

        End Select



    End Sub


    Private Sub RegesarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegesarToolStripMenuItem.Click
        NoCEDULATextBox.Text = 0
        Me.Close()
        frmPrincipal.Show()
    End Sub


    Private Sub formdatosEmpleadosActualizar_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        guardar()
        limipiarCampos()

    End Sub


    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        limipiarCampos()
    End Sub
    Sub limipiarCampos()
        Dim c As New Control

        For Each c In GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next
    End Sub


    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        guardar()
        limipiarCampos()

    End Sub



    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        guardar()
        limipiarCampos()

    End Sub

    Private Sub ActulizarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActulizarToolStripMenuItem.Click
        guardar()
        limipiarCampos()

    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        NoCEDULATextBox.Text = 0
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub btnBuscar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseHover
        ToolStripStatusLabel1.Text = "Buscar  un empleado guardado"
    End Sub


    Private Sub btnBuscar_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub NoCEDULATextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoCEDULATextBox.MouseHover
        ToolStripStatusLabel1.Text = "Cedula de la Persona que se va a guardar"
    End Sub

    Private Sub NoCEDULATextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoCEDULATextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub _1__APELLIDOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _1__APELLIDOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Primer Apellido del Trabajador"
    End Sub

    Private Sub _1__APELLIDOTextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _1__APELLIDOTextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub _2__APELLIDOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _2__APELLIDOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Segundo Apellido del Trabajador"
    End Sub

    Private Sub _2__APELLIDOTextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _2__APELLIDOTextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub NOMBRETextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.MouseHover
        ToolStripStatusLabel1.Text = "Nombre del Trabajador"
    End Sub

    Private Sub NOMBRETextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub E_MAILTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles E_MAILTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Email del trabajador"
    End Sub

    Private Sub E_MAILTextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles E_MAILTextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub CODPUESTOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Codigo del puesto que se le va a dar al trabajador"
    End Sub

    Private Sub CODPUESTOTextBox_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub FECHA_INGRESODateTimePicker_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseLeave, TelefonoTextBox.MouseLeave, NoCUENTATextBox.MouseLeave, N__HIJOSTextBox.MouseLeave, N__CONYUGESCheckBox.MouseLeave, GRADO_ACADEMICOTextBox.MouseLeave, FECHA_INGRESODateTimePicker.MouseLeave, DireccionTextBox.MouseLeave, CelularTextBox.MouseLeave, btnConsultar.MouseLeave, BANCO_A_DEPOSITARTextBox.MouseLeave
        ToolStripStatusLabel1.Text = "Status"
    End Sub

    Private Sub FECHA_INGRESODateTimePicker_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_INGRESODateTimePicker.MouseHover
        ToolStripStatusLabel1.Text = "Fecha de Ingreso Del trabajador"
    End Sub

    Private Sub N__CONYUGESCheckBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles N__CONYUGESCheckBox.MouseHover
        ToolStripStatusLabel1.Text = "Estado Civil del Trabajador"
    End Sub

    Private Sub N__HIJOSTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles N__HIJOSTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Numeros de hijos que tiene el Trabajador"
    End Sub

    Private Sub GRADO_ACADEMICOTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GRADO_ACADEMICOTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Grado Aademico que tiene el Trabajador"
    End Sub

    Private Sub BANCO_A_DEPOSITARTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BANCO_A_DEPOSITARTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Banco al que se le va a Depositar el Sueldo"
    End Sub

    Private Sub NoCUENTATextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoCUENTATextBox.MouseHover
        ToolStripStatusLabel1.Text = "Numero De Cuenta para Depositar"
    End Sub

    Private Sub TelefonoTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TelefonoTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Telefono Fijo del Trabajador "
    End Sub

    Private Sub CelularTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CelularTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Celular Personal del Trabajador"
    End Sub

    Private Sub DireccionTextBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DireccionTextBox.MouseHover
        ToolStripStatusLabel1.Text = "Direccion del Trabajador "
    End Sub

    Private Sub btnConsultar_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.MouseHover
        ToolStripStatusLabel1.Text = " Consultar los Empleados que estan Guardados"
    End Sub

    Private Sub txtDatoConsulta_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDatoConsulta.MouseHover
        ToolStripStatusLabel1.Text = "Cedula que desea Buscar"
    End Sub
   
    Private Sub CODPUESTOTextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles CODPUESTOTextBox.Validating
        Dim objTextBox As TextBox = CType(sender, TextBox)

        Try
            Dim codPuesto As Integer = Integer.Parse(objTextBox.Text)


            If Me.PuestosTableAdapter.FillByBuscarPorCodigo(frmPuestosActualizar.Planilla2DataSet.Puestos, codPuesto) <> 1 Then
                e.Cancel = True
                objTextBox.SelectAll()
                ErrorProvider1.SetError(objTextBox, "Codigo No se encuentra Registrado a Nigun Puesto")
            End If

        Catch ex As Exception

            e.Cancel = True
            objTextBox.SelectAll()
            ErrorProvider1.SetError(objTextBox, "Tiene que ser Numerico")


        End Try
    End Sub

    Private Sub CODPUESTOTextBox_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.Validated
        ErrorProvider1.Clear()
    End Sub
End Class