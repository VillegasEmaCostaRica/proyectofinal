﻿Public Class frmConsulataAjustes

    Private Sub AjustesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.AjustesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsulataAjustes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        'TODO: This line of code loads data into the 'Planilla2DataSet.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Select Case frmPrincipal.nomForm

            Case "Consultar"
                Try

                    '   If Me.AjustesTableAdapter.BuscarAjuste(Me.Planilla2DataSet.Ajustes, Integer.Parse(txtDato.Text)) = 0 Then
                    MsgBox("No Se Encontro Ningun Registro con este Codigo")
                    Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)
                    ' End If

                Catch ex As Exception
                    MsgBox("Verifique que sea Correcto El Codigo")
                End Try


            Case ("Eliminar")

                Try

                    If MsgBox("Esta Seguro de Eliminar este Ajuste", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                        'If Me.AjustesTableAdapter.DeleteAjuste(Integer.Parse(txtDato.Text)) = 1 Then
                        MsgBox("Se Elimino Correctamente Los Datos")
                        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)
                    Else
                        MsgBox("No Se Encontro Ningun Ajuste Con este Codigo")
                    End If



                    '  End If

                Catch ex As Exception
                    MsgBox("Verifique que sea Correcto El Codigo")
                End Try


            Case Else

        End Select

    End Sub
End Class