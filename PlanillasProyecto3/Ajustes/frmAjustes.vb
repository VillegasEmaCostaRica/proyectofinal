﻿Public Class frmAjustes
    Public Shared tipoFormularioPuestoValidar As String
    Public Shared tipoFormularioDatosPersonalesValidar As String
    Public Shared diferencia As Integer
    Public Shared diario As Integer
    Public Shared status As Boolean


    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmAjustes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter1.Fill(Me.Planilla2DataSet1.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSetPuestos.Puestos' table. You can move, or remove it, as needed.
        'TODO: This line of code loads data into the 'Planilla2DataSet.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)
        'TODO: This line of code loads data into the 'Planilla2DataSet.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        limpiaCajasTextoS()
       
        If CodPUESTO_NUEVOTextBox.Text = "" Then
            ComboBox2.Enabled = False
            Me.ToolTip1.SetToolTip(Me.ComboBox2, "Tiene que Seleccionar un puesto Nuevo")
        End If
        CelularTextBox.Text = ""
        NoCEDULATextBox.Focus()
        CODPUESTOTextBox.Text = ""
    End Sub

    Private Sub limpiaCajasTextoS()
        NoCEDULATextBox.Text = ""
        CODPUESTOTextBox.Text = ""
        CodPUESTO_NUEVOTextBox.Text = ""
        CodCedulaAjusteTextBox.Text = ""
        NOMBRE_COMPLETOTextBox.Clear()
        E_MAILTextBox.Clear()
        CODPUESTOTextBox.Clear()
        GRADO_ACADEMICOTextBox.Clear()
        GRADO_ACADEMICOTextBox1.Clear()
        GRADO_ACADREQUERIDOTextBox.Clear()
        TelefonoTextBox.Clear()
        CelularTextBox.Clear()
        DireccionTextBox.Clear()
        CODPUESTOTextBox.Text = ""
        DESCRIPCIONTextBox.Clear()
        DEPARTAMENTOTextBox.Clear()
        SALARIO_UNICOTextBox.Clear()
        EMPLEADO_CONFIANZACheckBox.Checked = False
        GRADO_ACADEMICOTextBox.Clear()

         DESCRIPCIONPUESTONUEVOTextBox.Clear()
        SALARIO_NUEVOTextBox.Clear()
        GRADO_ACADEMICOTextBox.Clear()



        DiferenciaTextBox.Clear()
        DiarioTextBox.Clear()
        TOTALAJUSTARTextBox.Clear()


    End Sub



    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        If NoCEDULATextBox.Focused = True AndAlso keyData = Keys.Tab Then


            Try

                If Me.DatosPersonalesTableAdapter.FillBy(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(NoCEDULATextBox.Text)) = False Then
                    MsgBox("No se encontro el emppleado")
                    NoCEDULATextBox.Focus()
                    CODPUESTOTextBox.Text = 0
                    CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)
                Else
                    CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)

                End If


            Catch ex As Exception
                MsgBox("Verifique los datos ingresados")
            End Try

            Try

                '  Me.PuestosTableAdapter.FillBy1(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
            Catch ex As Exception
                NoCEDULATextBox.Focus()
            End Try


        End If

        Return MyBase.ProcessCmdKey(msg, keyData)

    End Function

    Private Sub CodCedulaAjusteTextBox_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CodCedulaAjusteTextBox.Leave

        '  If Me.AjustesTableAdapter.BuscarAjuste(Me.Planilla2DataSet.Ajustes, Integer.Parse(CodCedulaAjusteTextBox.Text)) = 0 Then
        MsgBox("No se Encuentra Ningun Ajuste con este Codigo, Ver los Ajustes Guardados")
        CodCedulaAjusteTextBox.Focus()
        '   End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarEmpleado.Click


        Try

            If Me.DatosPersonalesTableAdapter.FillBy(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(NoCEDULATextBox.Text)) = False Then
                MsgBox("No se encontro el emppleado")
                NoCEDULATextBox.Focus()
                CODPUESTOTextBox.Text = 0
                CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)
            Else
                CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)

            End If


        Catch ex As Exception
            MsgBox("Verifique los datos ingresados")
        End Try

        Try

            '  Me.PuestosTableAdapter.FillBy1(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
        Catch ex As Exception
            NoCEDULATextBox.Focus()
        End Try


    End Sub

    Private Sub CODPUESTOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.PuestosTableAdapter.FillBy(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))

    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Try
            frmPuestosActualizar.tipoFormularioPuestoValidar = "Ajuste"
            frmPuestosConsulta.Show()
            Me.Hide()
            'Dim data As Integer = Integer.Parse(CODPUESTOTextBox.Text)

        Catch ex As Exception
            MsgBox("Verifique los datos")
        End Try
    End Sub


    Sub ajustarpPuesto()

        Try

            If ComboBox1.Text = "Si" Then

                If Integer.Parse(ComboBox2.Text) >= 7 Then

                    If Integer.Parse(ComboBox2.Text) <= 15 Then


                        If diferencia >= 0 Then
                            diferencia = Double.Parse(SALARIO_NUEVOTextBox.Text) - Double.Parse(SALARIO_UNICOTextBox.Text)
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Double.Parse(diferencia) / 15, "##,##")
                            DiarioTextBox.Text = diario
                            TOTALAJUSTARTextBox.Text = Math.Abs(diario * Double.Parse(ComboBox2.Text))
                        Else
                            diferencia = Double.Parse(SALARIO_UNICOTextBox.Text) - Double.Parse(SALARIO_NUEVOTextBox.Text)
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Double.Parse(diferencia) / 15, "##,##")
                            DiarioTextBox.Text = diario
                            TOTALAJUSTARTextBox.Text = Math.Abs(diario * Double.Parse(ComboBox2.Text))

                        End If
                    Else

                    End If
                Else
                    MsgBox("No cumple con Los dias Establecidos")
                End If

            ElseIf ComboBox1.Text = "No" Then
                status = False

                If Integer.Parse(ComboBox2.Text) > 7 Then

                    If Integer.Parse(ComboBox2.Text) <= 15 Then



                        If True Then

                            diferencia = (Double.Parse(SALARIO_NUEVOTextBox.Text) - Double.Parse(SALARIO_UNICOTextBox.Text)) * (50) / 100
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Integer.Parse(diferencia) / 15, "##,#")
                            DiarioTextBox.Text = diario

                            TOTALAJUSTARTextBox.Text = Math.Abs(diario * Double.Parse(ComboBox2.Text))
                        End If


                    Else

                    End If
                Else
                    DiferenciaTextBox.Text = "No hay calculo"

                End If
            End If
        Catch ex As Exception

        End Try
       
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try

            Me.AjustesTableAdapter.InsertQuery(Integer.Parse(CodCedulaAjusteTextBox.Text), Integer.Parse(CodPUESTO_NUEVOTextBox.Text), DESCRIPCIONPUESTONUEVOTextBox.Text, Integer.Parse(SALARIO_NUEVOTextBox.Text), GRADO_ACADEMICOTextBox.Text, status, CType(DiferenciaTextBox.Text, Integer), diario, Integer.Parse(ComboBox2.Text), Integer.Parse(TOTALAJUSTARTextBox.Text))
            MsgBox("Datos almacenados exitosamente")
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub

    
    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        frmPrincipal.Show()
        Me.Close()

    End Sub

    Private Sub ConsularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsularToolStripMenuItem.Click
        frmConsulataAjustes.Show()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try


            Me.AjustesTableAdapter.FillBy(Me.Planilla2DataSet.Ajustes, Integer.Parse(CodCedulaAjusteTextBox.Text))
            MsgBox("Empleado encontrado")


        Catch ex As Exception
            MsgBox("Error" + ex.Message)

        End Try

    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Me.Validate()
            Me.DatosPersonalesBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)
            MsgBox("Los datos modificados, se han actualizado")
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try

    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        Try
            Dim dato As Integer

            dato = InputBox("Ingrese el cod Empleado que desea borrar")

            If MsgBox("Desea borrar el registro", vbOK) = vbYes Then
                Me.AjustesTableAdapter.DeleteQuery(Integer.Parse(dato))
            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        tipoFormularioDatosPersonalesValidar = "Ajuste"
        frmDatosEmpleadoConsulta.Show()
        Me.Hide()
        
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        ajustarpPuesto()
    End Sub

    Private Sub CODPUESTOTextBox_TextChanged_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.TextChanged
        Me.PuestosTableAdapter.FillBy(Me.Planilla2DataSet.Puestos, Integer.Parse(CODIGO_DE_PUESTOTextBox.Text))
    End Sub
End Class