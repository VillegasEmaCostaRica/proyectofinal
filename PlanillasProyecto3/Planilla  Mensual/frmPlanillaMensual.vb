﻿Public Class frmPlanillaMensual
    Dim numeroCedula As String
    Dim numeroHijos As String
    Dim numeroConyugues As String
    Public datoBusqueda As String
    Public opcion As Boolean

    Dim sueldo As Double

    Private Sub frmPlanillaMensual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaBisemanal' table. You can move, or remove it, as needed.
        Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaBisemanal' table. You can move, or remove it, as needed.
        Me.PlanillaBisemanalTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaBisemanal)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet1.Impuestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet1.Deducciones)

        'TODO: This line of code loads data into the 'Planilla2DataSet1.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet1.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet1.PlanillaMensual' table. You can move, or remove it, as needed.
        Me.PlanillaMensualTableAdapter1.Fill(Me.Planilla2DataSet1.PlanillaMensual)
        Me.PlanillaMensualTableAdapter.Fill(Me.Planilla2DataSet.PlanillaMensual)

        N__HIJOSTextBox.Text = 0
        SALARIO_UNICOTextBox.Clear()
        FECHA_INGRESODateTimePicker.Text = ""
        SALARIO_DIARIOTextBox.Text = ""
        CCSSTextBox.Text = ""
        horasExtrasRegulares.Text = "0"
        comboxHorasExtrasDobles.Text = "0"
        LIQUIDO_A_PAGARTextBox.Text = "0"
        diazTrabajados.Text = "0"
        CESANTIATextBox.Text = "0"
        comboxHorasExtrasDobles.Text = "0"

    End Sub


    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Hide()
        frmConsultaDatosPersonales.Show()
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Me.Close()
        frmPrincipal.Show()
    End Sub

    Private Sub btnBuscar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
        frmConsultaDatosPersonales.Show()
    End Sub


    Sub buscarAjuste()
        Math.Abs(AjustesTableAdapter.FillByDatosAjuste(Me.Planilla2DataSet1.Ajustes, Integer.Parse(My.Forms.frmConsultaDatosPersonales.txtDatoBusqueda.Text)))
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
        frmConsultaDatosPersonales.Show()

    End Sub


    Sub buscarRegistro()
        Try
            numeroCedula = NoCEDULATextBox.Text
            DatosPersonalesTableAdapter.FillByEmployee(Me.Planilla2DataSet1.DatosPersonales, Integer.Parse(numeroCedula))
            PuestosTableAdapter.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
            Math.Abs(AjustesTableAdapter.FillByDatosAjuste(Me.Planilla2DataSet1.Ajustes, Integer.Parse(numeroCedula)))


        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        buscarRegistro()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
        SALARIO_UNICOTextBox.Clear()
        frmConsultaDatosPersonales.Show()

    End Sub

    Private Sub btnMostar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostar.Click
        buscarRegistro()
        añoImpuestoFiscal.Text = Year(Now) & vbTab & vbTab
        TextBox2.Text = Year(Now) & vbTab & vbTab
        CESANTIATextBox.Text = "0"
        Dim cedula As String
        cedula = NoCEDULATextBox.Text

        PlanillaMensualTableAdapter1.FillBy1(Me.Planilla2DataSet1.PlanillaMensual, Integer.Parse(cedula))

    End Sub

    Private Sub RadioButton1_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged, RadioButton2.CheckedChanged

        Dim radio As RadioButton
        radio = CType(sender, RadioButton)

        If radio Is RadioButton1 Then
            AjustesTableAdapter.FillByDatosAjuste(Me.Planilla2DataSet1.Ajustes, Integer.Parse(NoCEDULATextBox.Text))

        Else
            TOTALAJUSTARTextBox.Text = 0
        End If

    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PuestosTableAdapter.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
    End Sub


    Private Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtExtras.Text = Double.Parse(SALARIO_UNICOTextBox.Text) + Double.Parse(SALARIO_DIARIOTextBox.Text)

    End Sub


    Function ccss(ByVal salarioEmpleado As Double)
        Dim porcentaje As Double

        Try
            porcentaje = Fix(Integer.Parse(SALARIO_UNICOTextBox.Text) * 9) / 100

        Catch ex As Exception

        End Try


        Return Fix(porcentaje)


    End Function


    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        Try
            DeduccionesTableAdapter.FillByDeduccion(Me.Planilla2DataSet1.Deducciones, Integer.Parse(TextBox2.Text))
        Catch ex As Exception

        End Try

    End Sub



    Private Sub ComboBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diazTrabajados.TextChanged
        Try
            CESANTIATextBox.Text = Fix((Integer.Parse(SALARIO_UNICOTextBox.Text) * Integer.Parse(diazTrabajados.Text) / 360))
        Catch ex As Exception

        End Try
    End Sub



    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles añoImpuestoFiscal.TextChanged

        Try
            ImpuestosTableAdapter.FillByImpuestos(Me.Planilla2DataSet1.Impuestos, Integer.Parse(añoImpuestoFiscal.Text))

            If Integer.Parse(SALARIO_UNICOTextBox.Text) > 714000 Then

                IMP_RENTATextBox.Text = ((Integer.Parse(SALARIO_UNICOTextBox.Text) * 10) / 100) - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text)

                If N__CONYUGESCheckBox.Checked = True Then
                    IMP_RENTATextBox.Text = ((Integer.Parse(SALARIO_UNICOTextBox.Text) * 10) / 100) - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text) - Integer.Parse(CreditosFiscalesConyugeTextBox.Text)
                End If
                If Integer.Parse(SALARIO_UNICOTextBox.Text) < 1071000 Then
                    IMP_RENTATextBox.Text = (Integer.Parse(SALARIO_UNICOTextBox.Text) * 10) / 100 - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text)
                    If N__CONYUGESCheckBox.Checked = True Then
                        IMP_RENTATextBox.Text = ((Integer.Parse(SALARIO_UNICOTextBox.Text) * 10) / 100) - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text) - Integer.Parse(CreditosFiscalesConyugeTextBox.Text)
                    End If
                ElseIf Integer.Parse(SALARIO_UNICOTextBox.Text) > 1071000 Then
                    IMP_RENTATextBox.Text = (Integer.Parse(SALARIO_UNICOTextBox.Text) * 15) / 100 - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text)
                    If N__CONYUGESCheckBox.Checked = True Then
                        IMP_RENTATextBox.Text = ((Integer.Parse(SALARIO_UNICOTextBox.Text) * 10) / 100) - Integer.Parse(CreditosFiscalesHijoTextBox.Text) * Integer.Parse(N__HIJOSTextBox.Text) - Integer.Parse(CreditosFiscalesConyugeTextBox.Text)
                    End If

                End If

            End If

        Catch ex As Exception

        End Try
    End Sub



    Private Sub ComboBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboxHorasExtrasDobles.TextChanged, horasExtrasRegulares.TextChanged

        Try
            txtExtras.Text = (Integer.Parse(horasExtrasRegulares.Text) * Integer.Parse(SALARIO_DIARIOTextBox.Text)) + (Integer.Parse(comboxHorasExtrasDobles.Text) * Integer.Parse(SALARIO_DIARIOTextBox.Text) * 2)

        Catch ex As Exception

        End Try
    End Sub



    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        Try


            Dim resultado As Double

            Select Case CODPUESTOTextBox.Text
                Case 36

                    resultado = Integer.Parse(TextBox4.Text) * 0.015
                Case 21
                    resultado = Integer.Parse(TextBox4.Text) * 0.0175
                Case 31
                    resultado = Integer.Parse(TextBox4.Text) * 0.0175
                Case Else
                    resultado = Integer.Parse(TextBox4.Text) * 0.02
            End Select
            __ANUALTextBox.Text = resultado
        Catch ex As Exception

        End Try

    End Sub

    Private Sub TOTAL_DIAS_INCAPTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TOTAL_DIAS_INCAPTextBox.TextChanged

        Try
            If Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text) <= 3 Then
                SUBSIDIOTextBox.Text = (Integer.Parse(SALARIO_UNICOTextBox.Text) * 100) / 100

            End If

            If Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text) > 3 Then

                If ComboInstitucion.SelectedIndex = 0 Then
                    SUBSIDIOTextBox.Text = (Integer.Parse(SALARIO_UNICOTextBox.Text) * 40) / 100
                Else
                    SUBSIDIOTextBox.Text = (Integer.Parse(SALARIO_UNICOTextBox.Text) * 75) / 100
                End If

            End If

        Catch ex As Exception

        End Try


    End Sub

    Private Sub FechaINCAPFINDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaINCAPFINDateTimePicker.ValueChanged
        Dim fechaIncio As Date
        Dim fechaFinal As Date
        fechaIncio = FechaINCAPINIDateTimePicker.Text
        fechaFinal = FechaINCAPFINDateTimePicker.Text
        TOTAL_DIAS_INCAPTextBox.Text = DateDiff(DateInterval.Day, fechaIncio, fechaFinal)
    End Sub

    Private Sub FechaINCAPINIDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaINCAPINIDateTimePicker.ValueChanged

    End Sub

    Sub Limpiar_Contenedores()

        Dim c As New Control

        For Each c In Me.GroupBox1.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next
        For Each c In Me.GroupBox2.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next

        For Each c In Me.GroupBox3.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next
        For Each c In Me.GroupBox4.Controls

            If TypeOf c Is TextBox Then

                CType(c, TextBox).Clear()

            End If

        Next

        horasExtrasRegulares.Text = "0"
        comboxHorasExtrasDobles.Text = "0"
        diazTrabajados.Text = "0"
    End Sub


    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        guardar()

    End Sub

    Sub upDateRegistro()

        Try

        Catch ex As Exception
            MsgBox("Verifique los datos:" & ex.Message)
        End Try


    End Sub



    Sub guardar()
        Try

            Dim FechaPlanMensual As String
            FechaPlanMensual = FechaPlanMensualDateTimePicker.Text

            Dim NoCEDULA As Integer
            NoCEDULA = Integer.Parse(NoCEDULATextBox.Text)

            Dim Nombre As String
            Nombre = NOMBRE_COMPLETOTextBox.Text

            Dim cadena As String
            cadena = Replace(__ANUALTextBox.Text, ".", ",")
            Dim ANUALIDAD As Double
            ANUALIDAD = Double.Parse(ANUALIDADTextBox.Text)

            Dim SALARIOBRUTO As Double
            SALARIOBRUTO = Double.Parse(SALARIO_UNICOTextBox.Text)

            Dim SALARIODIARIO As Double
            SALARIODIARIO = Double.Parse(SALARIO_DIARIOTextBox.Text)

            Dim LIQUIDOAPAGAR As Double
            LIQUIDOAPAGAR = Double.Parse(LIQUIDO_A_PAGARTextBox.Text)

            Dim DIASTRABAJ As Integer
            DIASTRABAJ = Integer.Parse(diazTrabajados.Text)

            Dim DiasIncap As Integer
            DiasIncap = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            Dim institucionIncapacidad As Integer
            institucionIncapacidad = Integer.Parse(ComboInstitucion.SelectedIndex)

            Dim HORASEXTRASREGULAR As Integer
            HORASEXTRASREGULAR = Integer.Parse(horasExtrasRegulares.Text)

            Dim horasExtrasDobles As Integer
            horasExtrasDobles = Integer.Parse(comboxHorasExtrasDobles.Text)

            Dim TOTALDIASINCAP As Integer
            TOTALDIASINCAP = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            Dim FechaINCAPINI As String
            FechaINCAPINI = FechaINCAPINIDateTimePicker.Text

            Dim FechaINCAPFIN As String
            FechaINCAPFIN = FechaINCAPFINDateTimePicker.Text

            Dim Asiento As String
            Asiento = AsientoTextBox.Text

            Dim IMPRENTA As Double
            IMPRENTA = Double.Parse(IMP_RENTATextBox.Text)

            Dim EXTRAS As Double
            EXTRAS = Double.Parse(txtExtras.Text)

            Dim CESANTIA As Double
            CESANTIA = Double.Parse(CESANTIATextBox.Text)

            Dim SUBSIDIO As Double
            SUBSIDIO = Double.Parse(SUBSIDIOTextBox.Text)

            Dim codDed As Double
            codDed = Double.Parse(SumaTotalTextBox.Text)

            Dim ccss As Double
            ccss = (Double.Parse(CCSSTextBox.Text))

            If opcion = True Then
                PlanillaMensualTableAdapter1.InsertQuerys(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss)
                MsgBox("Planilla Mensual almacenada exitosamente")
                Limpiar_Contenedores()
            Else
                PlanillaBisemanalTableAdapter1.InsertQuery(FechaPlanMensual, NoCEDULA, Nombre, cadena, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss)
                MsgBox("Planilla Bisemanal almacenada exitosamente")
                Limpiar_Contenedores()

            End If
            Limpiar_Contenedores()
        Catch ex As Exception
            MsgBox("Verifique los datos:" & ex.Message)
        End Try

    End Sub
    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            Dim FechaPlanMensual As String
            FechaPlanMensual = FechaPlanMensualDateTimePicker.Text

            Dim NoCEDULA As Integer
            NoCEDULA = Integer.Parse(NoCEDULATextBox.Text)

            Dim Nombre As String
            Nombre = NOMBRE_COMPLETOTextBox.Text

            Dim ANUAL As Double
            ANUAL = Double.Parse(__ANUALTextBox.Text)

            Dim ANUALIDAD As Double
            ANUALIDAD = Double.Parse(ANUALIDADTextBox.Text)

            Dim SALARIOBRUTO As Double
            SALARIOBRUTO = Double.Parse(SALARIO_UNICOTextBox.Text)

            Dim SALARIODIARIO As Double
            SALARIODIARIO = Double.Parse(SALARIO_DIARIOTextBox.Text)

            Dim LIQUIDOAPAGAR As Double
            LIQUIDOAPAGAR = Double.Parse(LIQUIDO_A_PAGARTextBox.Text)

            Dim DIASTRABAJ As Integer
            DIASTRABAJ = Integer.Parse(diazTrabajados.Text)

            Dim DiasIncap As Integer
            DiasIncap = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            Dim institucionIncapacidad As Integer
            institucionIncapacidad = Integer.Parse(ComboInstitucion.SelectedIndex)

            Dim HORASEXTRASREGULAR As Integer
            HORASEXTRASREGULAR = Integer.Parse(horasExtrasRegulares.Text)

            Dim horasExtrasDobles As Integer
            horasExtrasDobles = Integer.Parse(comboxHorasExtrasDobles.Text)

            Dim TOTALDIASINCAP As Integer
            TOTALDIASINCAP = Integer.Parse(TOTAL_DIAS_INCAPTextBox.Text)

            Dim FechaINCAPINI As String
            FechaINCAPINI = FechaINCAPINIDateTimePicker.Text

            Dim FechaINCAPFIN As String
            FechaINCAPFIN = FechaINCAPFINDateTimePicker.Text

            Dim Asiento As String
            Asiento = AsientoTextBox.Text

            Dim IMPRENTA As Double
            IMPRENTA = Double.Parse(IMP_RENTATextBox.Text)

            Dim EXTRAS As Double
            EXTRAS = Double.Parse(txtExtras.Text)

            Dim CESANTIA As Double
            CESANTIA = Double.Parse(CESANTIATextBox.Text)

            Dim SUBSIDIO As Double
            SUBSIDIO = Double.Parse(SUBSIDIOTextBox.Text)

            Dim codDed As Double
            codDed = Double.Parse(SumaTotalTextBox.Text)

            Dim ccss As Double
            ccss = (Double.Parse(CCSSTextBox.Text))

            PlanillaMensualTableAdapter1.InsertQuerys(FechaPlanMensual, NoCEDULA, Nombre, ANUAL, ANUALIDAD, SALARIOBRUTO, SALARIODIARIO, LIQUIDOAPAGAR, DIASTRABAJ, DiasIncap, institucionIncapacidad, HORASEXTRASREGULAR, horasExtrasDobles, TOTALDIASINCAP, FechaINCAPINI, FechaINCAPFIN, Asiento, IMPRENTA, EXTRAS, CESANTIA, SUBSIDIO, codDed, ccss)
            MsgBox("Datos almacenados exitosamente")
        Catch ex As Exception
            MsgBox("Verifique los datos:" & ex.Message)
        End Try

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        frmPlanillaMensualConsultaS.Show()
        Me.Close()
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        frmPlanillaMensualConsultaS.Show()
        Me.Close()
    End Sub


    Private Sub ToolStripButton3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        guardar()
    End Sub

    Private Sub IngresaerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresaerToolStripMenuItem.Click
        guardar()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        frmPlanillaMensualConsultaS.Show()
        Me.Close()
    End Sub

    Private Sub FECHA_INGRESODateTimePicker_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_INGRESODateTimePicker.ValueChanged
        Dim data As Date
        data = FECHA_INGRESODateTimePicker.Text
        TextBox4.Text = DateDiff(DateInterval.Year, data, Date.Now())

    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        upDateRegistro()

    End Sub

    Private Sub CODPUESTOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.TextChanged
        Try
            PuestosTableAdapter.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(CODPUESTOTextBox.Text))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SALARIO_UNICOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox.TextChanged

        ANUALIDADTextBox.Text = "0"
        Try
            Me.sueldo = Integer.Parse(SALARIO_UNICOTextBox.Text)
            ANUALIDADTextBox.Text = sueldo * Double.Parse(__ANUALTextBox.Text)

            ANUALIDADTextBox.Text = sueldo * Double.Parse(__ANUALTextBox.Text)
            sueldo = Double.Parse(SALARIO_UNICOTextBox.Text)

            SALARIO_DIARIOTextBox.Text = Fix(Integer.Parse(SALARIO_UNICOTextBox.Text) / 30)
            CCSSTextBox.Text = ccss(Double.Parse(SALARIO_UNICOTextBox.Text))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SALARIO_UNICOTextBox1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALARIO_UNICOTextBox1.TextChanged, SALARIO_UNICOTextBox.TextChanged

    End Sub
End Class